import requests
from bs4 import BeautifulSoup

#Encabezado
encabezado={
    "user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
}

#Peticion
url="https://stackoverflow.com/questions"
respuesta=requests.get(url,headers=encabezado)

#Parseando el resultado
soup=BeautifulSoup(respuesta.text)

contenedor_preguntas=soup.find(id="questions")
lista_de_preguntas = contenedor_preguntas.find_all('div', class_="question-summary") 
# ENCONTRAR VARIOS ELEMENTOS POR TAG Y POR CLASE
for pregunta in lista_de_preguntas:
    """Metodo 1 tradicional
    print('Titulo:'+pregunta.find('h3').text)
    descripcion=pregunta.find(class_='excerpt').text
    print(descripcion.replace('\n','').replace('\r','').strip())
    """
    # METODO #2: APROVECHANDO EL PODER COMPLETO DE BEAUTIFUL SOUP
    contenedor_pregunta = pregunta.find('h3')
    texto_pregunta = contenedor_pregunta.text
    descripcion_pregunta = contenedor_pregunta.find_next_sibling('div')
    # TRAVERSANDO EL ARBOL DE UNA MENERA DIFERENTE
    texto_descripcion_pregunta = descripcion_pregunta.text
    texto_descripcion_pregunta = texto_descripcion_pregunta.replace('\n', '').replace('\t', '')
    print (texto_pregunta)
    print (texto_descripcion_pregunta)