import requests
from lxml import html

#Encabezado
encabezado={
    "user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
}

#Peticion
url="https://www.wikipedia.org/"
respuesta=requests.get(url,headers=encabezado)

parser=html.fromstring(respuesta.text)
#print(parser.get_element_by_id("js-link-box-en").text_content())
#idiomas=parser.xpath("//div[@class='central-featured']//strong/text()")

#idiomas=parser.xpath("//div[contains(@class,'central-featured-lang')]//strong/text()")
idiomas=parser.find_class('central-featured-lang')
for idioma in idiomas:
    print(idioma.text_content())