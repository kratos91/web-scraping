import random
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options

scrollingScript="""
    document.getElementsByClassName('section-layout section-scrollbox scrollable-y scrollable-show')[0].scroll(0, 20000)
"""

driver=webdriver.Chrome('./chromedriver')

driver.get('https://www.google.com/maps/place/Restaurante+El+Cardenal/@19.4337285,-99.137538,17z/data=!4m10!1m2!2m1!1sel+cardenal!3m6!1s0x85d1f92d51f67087:0xc076d1f0fb136d55!8m2!3d19.4337285!4d-99.1353493!9m1!1b1')

sleep(random.uniform(4.0,5.0))

SCROLLS=0
#Ciclo que hace los scrolling para obtner las opiniones
while (SCROLLS!=3):
    driver.execute_script(scrollingScript)
    sleep(random.uniform(5.0,6.0))
    SCROLLS+=1

reviews_restaurante=driver.find_elements(By.XPATH,'div[contains(@class,"section-review ripple-container")]')
for review in reviews_restaurante:
    user_link=review.find_element(By.XPATH,'./div[@class="section-review-title"]')
    try:
        user_link.click()
        driver.switch_to.window(driver.window_handles[1])
        boton_opiniones=WebDriverWait(driver,10).until(
            EC.presence_of_element_located((By.XPATH,'button[@class="section-tab-bar-tab ripple-container section-tab-bar-tab-unselected"]'))
        )

        boton_opiniones.click()
        WebDriverWait(driver,10).until(
            EC.presence_of_element_located((By.XPATH,'//dic[@class="section-layout section-scrollbox scrollable-y scrollable-show"]'))
        )

        USER_SCROLLS=0
        while(USER_SCROLLS!=3):
            driver.execute_script(scrollingScript)
            sleep(random.uniform(5,6))
            USER_SCROLLS+=1

        userReviews=driver.find_elements((By.XPATH,'//div[contains(@class,"section-review ripple-container")]'))

        for userReview in userReviews:
            comentario=userReview.find_element(By.XPATH,'.//span[@class="section-review-text"]').text
            rating=userReview.find_element(By.XPATH,'.//span[@class="section-review-stars"]').get_attribute('aria-label')

            print(comentario)
            print(rating)
        driver.close()
        driver.switch_to.window(driver.window_handles[0])
    except Exception as e:
        print(e)
        driver.close()
        driver.switch_to.window(driver.window_handles[0])

        

