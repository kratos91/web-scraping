from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

driver=webdriver.Chrome('./chromedriver')

driver.get('https://www.olx.com.ec')
driver.refresh()

for i in range(3):
    try:
        #Inicializa el boton para cargar mas datos
        boton=WebDriverWait(driver,10).until(
            EC.presence_of_element_located((By.XPATH,'//button[@data-aut-id="btnLoadMore"]'))
        )
        boton.click()
        #Espero a que la informacion se cargue dinamicamente
        WebDriverWait(driver,10).until(
            EC.presence_of_all_elements_located((By.XPATH,'//li[@data-aut-id="itemBox"]//span[@data-aut-id="itemPrice"]'))
        )
    except:
        break

#Todos los anuncios en una lista
productos=driver.find_elements_by_xpath('//li[@data-aut-id="itemBox"]')

for producto in productos:
    precio=producto.find_element_by_xpath('.//span[@data-aut-id="itemPrice"]').text
    print(precio)
    descripcion=producto.find_element_by_xpath('.//span[@data-aut-id="itemTitle"]').text
    print(descripcion)
  
