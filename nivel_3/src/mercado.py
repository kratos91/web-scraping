from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options



driver=webdriver.Chrome('./chromedriver')

driver.get('https://vehiculos.mercadolibre.com.mx/mustang_SHORT*VERSION_1052767')

while True:
    links_productos=driver.find_elements(By.XPATH,'//a[@class="ui-search-result__content ui-search-link"]')
    links_de_la_pagina=[]

    for tag_a in links_productos:
        links_de_la_pagina.append(tag_a.get_attribute('href'))

    for link in links_de_la_pagina:
        try:
            driver.get(link)
            titulo=driver.find_element(By.XPATH,'//h1[contains(@class,"item-title")]').text
            precio=driver.find_element(By.XPATH,'//span[@class="price-tag-fraction"]').text
            print(titulo)
            print(precio)
            driver.back()
        except Exception as e:
            driver.back()
            print(e)

    try:
        boton_siguiente=driver.find_element(By.XPATH,'//span[text()="Siguiente"]')
        boton_siguiente.click()
    except:
        print("Se acabaron las paginas")
        break


