import requests
import pandas as pd

cursos_totales=[]

headers={
    'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    'referer':'https://www.udemy.com/'
}

url_api='https://www.udemy.com/api-2.0/discovery-units/?context=featured&from=0&page_size=6&item_count=12&source_page=logged_in_homepage&locale=es_ES&currency=mxn&navigation_locale=en_US&skip_price=true'

response=requests.get(url_api,headers=headers)

data=response.json()

cursos=data['units']

for curso in cursos:
    for item in curso['items']:
        cursos_totales.append({
            "titulo":item['headline'],
            "rating":item['avg_rating'],
            "nivel":item['instructional_level_simple'],
            "creado":item['created']
        })

df=pd.DataFrame(cursos_totales)
df.to_json("cursos_udemy.json")
