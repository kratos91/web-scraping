from selenium import webdriver
from selenium.webdriver.chrome.options import Options

opts=Options()
opts.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")

driver=webdriver.Chrome('./chromedriver',chrome_options=opts)

url='http://google.com/recaptcha/api2/demo'
driver.get(url)

try:
    driver.switch_to.frame(driver.find_element_by_xpath('//iframe'))
    captcha=driver.find_element_by_xpath('//div[@class="recaptcha-checkbox-border"]')
    captcha.click()
    
    input()

    driver.switch_to.default_content()
    submit=driver.find_element_by_xpath('//input[@id="recaptcha-demo-submit"]')
    submit.click()
except Exception as e:
    print(e)
    
final=driver.find_element_by_xpath('//div[@class="recaptcha-success"]').text
print(final)