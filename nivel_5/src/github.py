import requests
from lxml import html

headers={
    "user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
}

login_form_url="https://github.com/login"

session=requests.Session()

login_form_res=session.get(login_form_url,headers=headers)

parser=html.fromstring(login_form_res.text)
token_especial=parser.xpath('//input[@name="authenticity_token"]/@value')

login_url="https://github.com/session"

login_data={
    "login":"user",
    "password":"contra",
    "commit":"Sign in",
    "authenticity_token":token_especial
}

session.post(login_url,data=login_data,headers=headers)

data_url="https://github.com/emiliano080591?tab=repositories"
respuesta=session.get(
    data_url,headers=headers
)

par=html.fromstring(respuesta.text)
repositorios=par.xpath('//h3[@class="wb-break-all"]/a/text()')

for repositorio in repositorios:
    print(repositorio)