from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

opts=Options()
opts.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")

driver=webdriver.Chrome('./chromedriver',options=opts)
driver.get('https://www.saes.cenlexz.ipn.mx/')

user="boleta"
password="contra"

btn_continuar=driver.find_element(By.XPATH,'//button[@id="details-button"]')
btn_continuar.click()

link_continuar=driver.find_element(By.XPATH,'//a[@id="proceed-link"]')
link_continuar.click()

try:
    driver.switch_to.frame(driver.find_element_by_xpath('//iframe[@name="_centro"]'))

    input_user=driver.find_element(By.XPATH,'//input[@id="nombre"]')
    input_pass=driver.find_element(By.XPATH,'//input[@id="email"]')

    input_user.send_keys(user)
    input_pass.send_keys(password)

    boton_login=driver.find_element(By.XPATH,'//input[@name="enviar"]')

    boton_login.click()
    driver.switch_to.default_content()

    driver.switch_to.frame(driver.find_element_by_xpath('//iframe[@name="_centro"]'))
    link_re=driver.find_element(By.XPATH,'//a[contains(text(),"Reinscripción al Bimestre")]')
    link_re.click()
except Exception as e:
    print(e)
