import requests
from PIL import Image
from time import sleep
import io
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

driver=webdriver.Chrome('./chromedriver')

driver.get('https://www.olx.com.ec')
driver.refresh()

for i in range(2):
    try:
        #Inicializa el boton para cargar mas datos
        boton=WebDriverWait(driver,10).until(
            EC.presence_of_element_located((By.XPATH,'//button[@data-aut-id="btnLoadMore"]'))
        )
        boton.click()
        #Espero a que la informacion se cargue dinamicamente
        WebDriverWait(driver,10).until(
            EC.presence_of_all_elements_located((By.XPATH,'//li[@data-aut-id="itemBox"]//span[@data-aut-id="itemPrice"]'))
        )
    except:
        break

driver.execute_script("window.scrollTo({top:0,behavior:'smooth'});")
sleep(5)
driver.execute_script("window.scrollTo({top:20000,behavior:'smooth'});")
sleep(5)
#Todos los anuncios en una lista
productos=driver.find_elements_by_xpath('//li[@data-aut-id="itemBox"]')
i=0
for producto in productos:
    precio=producto.find_element_by_xpath('.//span[@data-aut-id="itemPrice"]').text
    print(precio)
    descripcion=producto.find_element_by_xpath('.//span[@data-aut-id="itemTitle"]').text
    print(descripcion)

    try:
        url=producto.find_element_by_xpath('.//img').get_attribute('src')
        image_content=requests.get(url).content
        image_file=io.BytesIO(image_content)
        imagen=Image.open(image_file).convert('RGB')
        path='./imagenes/'+str(i)+'.jpg'
        with open(path,'wb') as f:
            imagen.save(f,"JPEG",qualify=85)
    except:
        print('ERROR')
    i+=1
  
