from twisted.internet import reactor
from twisted.internet.task import LoopingCall
from scrapy.crawler import CrawlerRunner
from scrapy.crawler import CrawlerProcess
from scrapy.spiders import Spider

class ExtractorClima(Spider):
    name="Clima"
    custom_settings={
        'USER_AGENT':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'LOG_ENABLED':False
    }

    start_urls=[
        "https://www.accuweather.com/es/mx/mexico-city/1-242560_16_al/weather-forecast/1-242560_16_al",
        "https://www.accuweather.com/es/mx/monterrey/244681/weather-forecast/244681"
    ]

    def parse(self,reponse):
        ciudad=response.xpath('//h1[@class="header-loc"]/text()').get()
        current=response.xpath('//div[@class="cur-con-weather-card__panel"]//div[@class="temp"]/text()').get()
        real_feel=response.xpath('//div[@class="cur-con-weather-card__panel"]//div[@class="real-feel"]').get()
        
        current=current.replace('°','').replace('\n ','').replace('\t ','').strip()
        real_feel=real_feel.replace('RealFeel®','').replace('°','').replace('\n ','').replace('\t ','').strip()

        print(ciudad)
        print(current)
        print(real_feel)
        print()

        f.open("./datos_clima_scrapy.csv","a")
        f.write(ciudad+","+current+","+real_feel+"\n")
        f.close()

""" PARA CORRER SCRAPY COMO SCRIPT NORMAL DE PYTHON
process=CrawlerProcess()
process.crawl(ExtractorClima)
process.start()
"""

#PARA CORRER EL SCRIPT CADA DETERMINADO TIEMPO
runner=CrawlerRunner()
task=LoopingCall(lambda:runner.crawl(ExtractorClima))
task.start(60)#en segundos
reactor.run()