## Web scraping de paginas como mercadolibre,stackoverflow,etc.

### Librerias ocupadas
* requests
* lxml
* beautifulsoup4
* selenium
* Pillow
* scrapy

### Para correr scrapy
scrapy runspider stack_scrapy.py -o prueba.csv -t csv

### Para activar el virtual env
source venv/bin/activate

### Driver de selenium para google chrome
https://sites.google.com/a/chromium.org/chromedriver/downloads