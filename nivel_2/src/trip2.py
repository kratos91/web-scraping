from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import CrawlSpider,Rule
from scrapy.selector import Selector
from scrapy.loader.processors import MapCompose
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader

class Opinion(Item):
    titulo=Field()
    calificacion=Field()
    contenido=Field()
    autor=Field()

class TripAdvisor(CrawlSpider):
    name='Trip advisor'
    custom_settings={
        'USER_AGENT':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'CLOSESPIDER_PAGECOUNT':100
    }

    download_delay=1

    allowed_domains=['tripadvisor.com.mx']

    start_urls=['https://www.tripadvisor.com.mx/SmartDeals-g150768-Mexico-Hotel-Deals.html']

    rules=(
        #Paginacion de hoteles
        Rule(
            LinkExtractor(
                allow=r'-oa\d+-'
            ),follow=True
        ),
        #Detalle de hoteles
        Rule(
            LinkExtractor(
                allow=r'/Hotel_Review-',
                restrict_xpaths=['//div[@id="taplc_hsx_hotel_list_lite_smart_deals_responsive_sponsored_0"]//a[@data-clicksource="HotelName"]']
            ),follow=True
        ),
        #Paginacion de opiniones dentro de un hotel
        Rule(
            LinkExtractor(
                allow=r'-or\d+-'
            ),follow=True
        ),
        #Detalle de perfil de usuario
        Rule(
            LinkExtractor(
                allow=r'/Profile/',
                restrict_xpaths=['//div[@data-test-target="reviews-tab"]//a[contains(@class,"ui-header")]']
            ),follow=True,callback='parse_opinion'
        ),
    )

    def obtenerCal(self,texto):
        calificacion=texto.split("_")[-1]
        return calificacion


    def parse_opinion(self,response):
        sel=Selector(response)
        opiniones=sel.xpath('//div[contains(@class,"ui_card section")]/div/div/text()')

        autor=sel.add_xpath('//h1/span/text()')
        for opinion in opiniones:
            item=ItemLoader(Opinion(),opinion)

            item.add_xpath('autor',autor)
            item.add_xpath('titulo','.//div[@class="_3IEJ3tAK _2K4zZcBv"]')
            item.add_xpath('contenido','.//q/text()')
            item.add_xpath('calificacion','.//div[@class="_1VhUEi8g _2K4zZcBv"]/span/@class',MapCompose(self.obtenerCal))
            
            yield item.load_item()