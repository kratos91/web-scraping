from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import CrawlSpider,Rule
from scrapy.selector import Selector
from scrapy.loader.processors import MapCompose
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader

class Departamento(Item):
    nombre=Field()
    direccion=Field()

class Inmuebles(CrawlSpider):
    name='Inmuebles24'
    custom_settings={
        'USER_AGENT':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'CLOSESPIDER_PAGECOUNT':30
    }

    download_delay=1

    allowed_domains=['inmuebles24.com']

    start_urls=[
        'https://www.inmuebles24.com/desarrollos-o-departamentos.html',
        'https://www.inmuebles24.com/desarrollos-o-departamentos-pagina-2.html',
        'https://www.inmuebles24.com/desarrollos-o-departamentos-pagina-3.html',        
    ]

    rules=(
        Rule(
            LinkExtractor(
                allow=r'/propiedades/'
            ),follow=True,callback='parse_depa'
        ),
    )

    def parse_depa(self,response):
        sel=Selector(response)
        item=ItemLoader(Departamento(),sel)
        item.add_xpath('nombre','//div[@class="section-title"]./h1/text()')
        item.add_xpath('direccion','//div[@class="section-location"]./*/text()')

        yield item.load_item()

