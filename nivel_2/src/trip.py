from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import CrawlSpider,Rule
from scrapy.selector import Selector
from scrapy.loader.processors import MapCompose
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader

class Hotel(Item):
    nombre=Field()
    precio=Field()
    descripcion=Field()
    amenities=Field()

class TripAdvisor(CrawlSpider):
    name='Hoteles'

    custom_settings={
        'USER_AGENT':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'CLOSESPIDER_ITEMCOUNT':24
    }

    start_urls=['https://www.tripadvisor.com.mx/SmartDeals-g150768-Mexico-Hotel-Deals.html']

    download_delay=2
    
    rules=(
        Rule(
            LinkExtractor(
                allow=r'/Hotel_Review-'
            ),follow=True,callback="parse_hotel"
        ),
    )

    def quitarSimbolo(self,texto):
        nuevoTexto=texto.replace("$","")
        nuevoTexto=nuevoTexto.replace("\n","").replace("\t","").replace("\r","")
        return nuevoTexto

    def parse_hotel(self,response):
        sel=Selector(response)
        item=ItemLoader(Hotel(),sel)

        item.add_xpath('nombre','//h1[@id="HEADING"]/text()')
        item.add_xpath('precio','//div[@data-sizegroup="hr_chevron_prices"]/text()',MapCompose(self.quitarSimbolo))
        item.add_xpath('descripcion','//div[contains(@class,"ssr-init-26f")]//p/text()')
        item.add_xpath('amenities','//div[@data-test-target="amenity_text"]/text()')

        yield item.load_item()