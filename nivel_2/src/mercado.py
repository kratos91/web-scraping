from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import CrawlSpider,Rule
from scrapy.selector import Selector
from scrapy.loader.processors import MapCompose
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader

class Consola(Item):
    titulo=Field()
    precio=Field()
    descripcion=Field()

class MercadoLibre(CrawlSpider):
    name='mercadoLibre'
    custom_settings={
        'USER_AGENT':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'CLOSESPIDER_PAGECOUNT':20
    }

    download_delay=1

    allowed_domains=['videojuegos.mercadolibre.com.mx','www.mercadolibre.com.mx']
    
    start_urls=['https://videojuegos.mercadolibre.com.mx/consolas/playstation-4/']

    rules=(
        #Paginación
        Rule(
            LinkExtractor(
                allow=r'/_Desde_'
            ),follow=True
        ),
        #Detalle de los productos
        Rule(
            LinkExtractor(
                allow=r'/sony-playstation-4'
            ),follow=True,callback='parse_items'
        ),
    )

    def parse_items(self,response):
        item=ItemLoader(Consola(),response)
        item.add_xpath('titulo','//h1[@class="ui-pdp-title"]/text()',MapCompose(lambda i: i.replace('\n', ' ').replace('\r', ' ').strip()))
        item.add_xpath('precio','//span[@class="price-tag-fraction"]/text()')
        item.add_xpath('descripcion','//p[@class="ui-pdp-description__content"]/text()',MapCompose(lambda i: i.replace('\n', ' ').replace('\r', ' ').strip()))

        yield item.load_item()
